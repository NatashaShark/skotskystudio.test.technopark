﻿function show_dropdown_list(parent) {
    parent.getElementsByTagName("ul")[0].hidden = false;
    parent.onclick = function () {
        hide_dropdown_list(this);
    }    
}

function hide_dropdown_list(parent) {
    parent.getElementsByTagName("ul")[0].hidden = true;
    parent.onclick = function () {
        show_dropdown_list(this);
    }
}

function show_menu(sender) {
    var menu = document.getElementById("menu");
    menu.classList.add("visible");

    sender.onclick = function () {
        hide_menu(this);
    }
}

function hide_menu(sender) {
    var menu = document.getElementById("menu");
    menu.classList.remove("visible");

    sender.onclick = function () {
        show_menu(this);
    }
}

function show_search(sender) {
    var search = document.getElementById("search");
    search.classList.add("visible");
    search.getElementsByClassName("close")[0].classList.add("visible");

    sender.onclick = function () {
        hide_search(this);
    }
}

function hide_search(sender) {
    var search = document.getElementById("search");
    search.classList.remove("visible");
    search.getElementsByClassName("close")[0].classList.remove("visible");

    sender.onclick = function () {
        show_search(this);
    }
}

function show_filtres(sender) {
    var filtres = document.getElementById("filtres");
    filtres.classList.add("visible");
    filtres.getElementsByClassName("close")[0].classList.add("visible");
}

function hide_filtres(sender) {
    var filtres = document.getElementById("filtres");
    filtres.classList.remove("visible");
    filtres.getElementsByClassName("close")[0].classList.remove("visible");
}

function switch_on_single(elem) {
    var all = elem.parentNode.parentNode.getElementsByTagName("a");
    for (var i = 0; i < all.length; i++) {
        all[i].classList.remove("active");
    }
    elem.classList.add("active");
    elem.onclick = function () {
        switch_off_single(this);
    }
}

function switch_off_single(elem) {
    
    elem.classList.remove("active");
    elem.onclick = function () {
        switch_on_single(this);
    }
}

function switch_on(elem) {
    elem.classList.add("active");
    elem.onclick = function () {
        switch_off(this);
    }
}

function switch_off(elem) {

    elem.classList.remove("active");
    elem.onclick = function () {
        switch_on(this);
    }
}


function show_infocard(id) {
    /*getting information from server by id*/

    var card = document.getElementById("infocard");
    card.getElementsByClassName("close")[0].classList.add("visible");
    card.classList.add("visible");
}

function hide_infocard() {

    var card = document.getElementById("infocard");
    card.classList.remove("visible");
}